#!/bin/bash

CUR_DIR=$PWD
BB_DIR=${CUR_DIR}/BB
YOCTO=${BB_DIR}/yocto
POKY_DIR=${YOCTO}/poky
DEV_CONF=${BB_DIR}/conf
BB_BUILD=${POKY_DIR}/build_bbb
BUILD_CONF=${BB_BUILD}/conf


Clear; clear;

if [ ! -d ${BB_DIR} ]
then
    mkdir ${BB_DIR}
    mkdir ${DEV_CONF}
fi

cd ${BB_DIR}

if [ "$1" = "dep" ]
then
    sudo apt-get install emacs tree
    sudo apt-get install gawk wget git-core diffstat unzip texinfo gcc-multilib \
         build-essential chrpath  libsdl1.2-dev
    exit 0
elif [ "$1" = "clean" ]
then
    rm -rf ${BB_BUILD}
    exit 0
elif [ "$1" = "build" ]
then
    cd ${POKY_DIR} && source oe-init-build-env ${BB_BUILD}
    if [ -f ${DEV_CONF}/local.conf ]
    then
	cp ${DEV_CONF}/* ${BUILD_CONF}
    else
	cp -r ${BUILD_CONF}/* ${DEV_CONF}
    fi

    bitbake core-image-sato
    exit 0
elif [ "$1" = "sd" ]
then
    SDB1=/dev/sdb1
    SDB2=/dev/sdb2
    SDB=/dev/sdb
    IMG_DIR=${BB_BUILD}/tmp/deploy/images/beaglebone/
    BOOT_PT=/media/$USER/BOOT
    ROOT_PT=/media/$USER/ROOT

    # sudo umount /dev/${SDB1} /DEV/${SDB2} /dev/${SDB}
    # sudo fdisk ${SDB}
    # sudp mkfs.vfat -n "BOOT" ${SDB1}
    # sudo mkfs.ext4 -L "ROOT" ${SDB2}

    if [ ! -d ${BOOT_PT} -a  ! -d ${ROOT_PT} ]
    then
        echo "-------------------------"
        sudo mkdir ${BOOT_PT} ${ROOT_PT}
    fi

    sudo umount ${SDB1}
    sudo umount ${SDB2}
    sudo mount ${SDB1} ${BOOT_PT}
    sudo mount ${SDB2} ${ROOT_PT}

    #exit 0;
    sudo rm -rf ${BOOT_PT}/* ${ROOT_PT}/*

    cd ${IMG_DIR}
    # Copy the boot image MLO U-BOOT
    sudo cp MLO ${BOOT_PT}
    sudo cp u-boot.img ${BOOT_PT}

    # Copy the kernel to the Root partition
    sudo cp uImage ${BOOT_PT}

    # If Building core-image-minimal uncoment this
    # sudo cp am335x-boneblack.dtb ${BOOT_PT}

    # Decompress the  tar.bz2 files system into the ROOT
    sudo tar -xf core-image-sato-beaglebone.tar.bz2 -C ${ROOT_PT}

    sudo umount ${SDB1}
    sudo umount ${SDB2}
    exit 0
elif [ "$1" = "yocto" ]
then
    echo "--------- DOING SOME REAL WORK ----------"

    # Create the yocto support for BB
    if [ ! -d ${YOCTO} ]
    then
	mkdir ${YOCTO}
    fi
    cd ${YOCTO}

    git clone -b daisy git://git.yoctoproject.org/poky.git
    cd ${POKY_DIR}
fi
echo "done"
